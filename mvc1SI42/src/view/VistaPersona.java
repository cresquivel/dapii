package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JScrollPane;

public class VistaPersona extends JFrame {

	private JPanel contentPane;
	private JTextField CURPtxt;
	private JTextField Nombretxt;
	private JTextField ApellidoPtxt;
	private JTextField ApellidoMtxt;
	private JButton btnGuardar;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					VistaPersona frame = new VistaPersona();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public VistaPersona() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 371);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		CURPtxt = new JTextField();
		CURPtxt.setBounds(120, 36, 153, 20);
		contentPane.add(CURPtxt);
		CURPtxt.setColumns(10);
		
		Nombretxt = new JTextField();
		Nombretxt.setBounds(120, 72, 153, 20);
		contentPane.add(Nombretxt);
		Nombretxt.setColumns(10);
		
		ApellidoPtxt = new JTextField();
		ApellidoPtxt.setBounds(120, 103, 153, 20);
		contentPane.add(ApellidoPtxt);
		ApellidoPtxt.setColumns(10);
		
		ApellidoMtxt = new JTextField();
		ApellidoMtxt.setBounds(120, 139, 153, 20);
		contentPane.add(ApellidoMtxt);
		ApellidoMtxt.setColumns(10);
		
		JLabel lblCurp = new JLabel("CURP");
		lblCurp.setBounds(37, 39, 46, 14);
		contentPane.add(lblCurp);
		
		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setBounds(37, 75, 46, 14);
		contentPane.add(lblNombre);
		
		JLabel lblApellidoP = new JLabel("Apellido P");
		lblApellidoP.setBounds(37, 106, 66, 14);
		contentPane.add(lblApellidoP);
		
		JLabel lblApellidoM = new JLabel("Apellido M");
		lblApellidoM.setBounds(37, 142, 66, 14);
		contentPane.add(lblApellidoM);
		
		btnGuardar = new JButton("Guardar");
		btnGuardar.setBounds(307, 35, 89, 23);
		contentPane.add(btnGuardar);
		
//		JButton btnModificar = new JButton("Modificar");
//		btnModificar.setBounds(307, 71, 89, 23);
//		contentPane.add(btnModificar);
//		
//		JButton btnEliminar = new JButton("Eliminar");
//		btnEliminar.setBounds(307, 102, 89, 23);
//		contentPane.add(btnEliminar);
//		
//		JScrollPane tablaPersona = new JScrollPane();
//		tablaPersona.setBounds(35, 199, 294, 123);
//		contentPane.add(tablaPersona);
	}

	public JPanel getContentPane() {
		return contentPane;
	}

	public JTextField getCURPtxt() {
		return CURPtxt;
	}

	public JTextField getNombretxt() {
		return Nombretxt;
	}

	public JTextField getApellidoPtxt() {
		return ApellidoPtxt;
	}

	public JTextField getApellidoMtxt() {
		return ApellidoMtxt;
	}

	public JButton getBtnGuardar() {
		return btnGuardar;
	}
	
	public void limpiarCampos(){
		this.CURPtxt.setText("");
		this.Nombretxt.setText("");
		this.ApellidoPtxt.setText("");
		this.ApellidoMtxt.setText("");
	}
	
	
}
