package principal;

import controller.GuardarPersonaController;
import model.daos.IPersonaDAO;
import model.daos.PersonaDAO;
import model.daos.PersonaOracleDAO;
import view.VistaPersona;
import view.VistaPersonaGreen;

public class ProyectoPersona {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		IPersonaDAO dao = new PersonaDAO();
		VistaPersona vista = new VistaPersonaGreen();
		vista.setVisible(true);
		GuardarPersonaController controller 
		= new GuardarPersonaController(dao, vista);
	}
}
