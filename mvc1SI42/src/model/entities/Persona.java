package model.entities;

public class Persona {
	
	// definimos atributos de la clase
	// es decir la informaci�n que debe tener la entidad
	// los objetos de modelo son llamados tambi�n entidades
	private int id;
	private String curp;
	private String nombre;
	private String apellidoPaterno;

	private String apellidoMaterno;
	
	// podemos generar uno o varios constructores
	// por lo general siempre tienen el constructor
	// por default
	public Persona(){
		
	}
	
	

	public Persona(String curp, String nombre, String apellidoPaterno, String apellidoMaterno) {
		super();
		this.curp = curp;
		this.nombre = nombre;
		this.apellidoPaterno = apellidoPaterno;
		this.apellidoMaterno = apellidoMaterno;
	}

	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getCurp() {
		return curp;
	}

	public void setCurp(String curp) {
		this.curp = curp;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidoPaterno() {
		return apellidoPaterno;
	}

	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}

	public String getApellidoMaterno() {
		return apellidoMaterno;
	}

	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}
	
	

}
