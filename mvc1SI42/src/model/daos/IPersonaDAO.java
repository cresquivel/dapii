package model.daos;

import java.sql.SQLException;
import java.util.List;

import model.entities.Persona;

public interface IPersonaDAO {
	
	public void agregar(Persona p) throws SQLException;
	
	public void modificar(Persona p);
	
	public void eliminar(Persona p);
	
	public Persona buscarPorCurp(String curp);
	
	public List<Persona> buscarPorNombre(String nombre);
	
	public List<Persona> buscarTodos();

}
