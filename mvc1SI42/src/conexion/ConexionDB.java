package conexion;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConexionDB {
 private static Connection con=null;//singleton
  
 public static Connection getConnection(){
	  try{
		if(con == null){
			String driver="com.mysql.jdbc.Driver";
			String url="jdbc:mysql://localhost:3307/prueba?autoReconect=true";
		    String pwd="";
		    String usr="root";
		    Class.forName(driver);
		    con = (Connection) DriverManager.getConnection(url,usr,pwd);
		    System.out.println("Conexion Exitosa");
		}
		  
	  }catch(ClassNotFoundException | SQLException ex){
		  ex.printStackTrace();
		  
	  }
	  return con;
	  
  }
 
 
}
