package controller;

import java.sql.SQLException;

import javax.swing.JOptionPane;

import model.daos.IPersonaDAO;
import model.entities.Persona;
import view.VistaPersona;

public class GuardarPersonaController {
	IPersonaDAO dao;
	VistaPersona vista;

	public GuardarPersonaController(IPersonaDAO dao, VistaPersona vista) {
		this.dao = dao;
		this.vista = vista;

		this.vista.getBtnGuardar().addActionListener(e -> guardarPersona());
	}

	public void guardarPersona() {

		int confirmacion = JOptionPane.showConfirmDialog(null, "¿Quieres guardar la información?");
		if (JOptionPane.OK_OPTION == confirmacion) {
			String curp = vista.getCURPtxt().getText();
			String nombre = vista.getNombretxt().getText();
			String aPat = vista.getApellidoPtxt().getText();
			String aMat = vista.getApellidoMtxt().getText();
			Persona p = new Persona(curp, nombre, aPat, aMat);

			
			try{
				dao.agregar(p);
				JOptionPane.showMessageDialog(null, "Transacción Exitosa");
				this.vista.limpiarCampos();
			}catch(SQLException e){
				JOptionPane.showMessageDialog(null, "Error al agregar a la Persona"+e);
			}

		}

	}

}
